var http = require('http');

var Person = function (voornaam, achternaam, email) {
    this._voornaam = voornaam;
		this._achternaam = achternaam;
		this._email = email;
};

var peter = new Person('Peter', "Kassenaar", "info@gmail.com");

var server = http.createServer(function (request, response) {
		if (request.url == "/") {
				response.writeHead(200, {'Content-Type': 'application/JSON'});
				response.write(JSON.stringify(peter));
		} else {
				response.writeHead(404, {'Content-Type': 'application/JSON'});
		}
		response.end();
})

server.listen(3000);
