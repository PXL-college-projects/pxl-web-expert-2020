import { Component, OnInit } from '@angular/core';
import { Contact } from './models/contact.model';
import { ContactService } from './services/contact.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  contactList: Contact[] = [];

  ngOnInit() {
    this.contactList = this.service.getContactList();
  }

  handleData(event: Contact) {
    console.log('Recieved data!', event);
  }

  constructor(private service: ContactService) { }

  createContact(event: Contact) {
    this.service.addContact(event);
    this.contactList = this.service.getContactList();
  }

  handleUpdate() {
    this.contactList = this.service.getContactList();
  }
}
