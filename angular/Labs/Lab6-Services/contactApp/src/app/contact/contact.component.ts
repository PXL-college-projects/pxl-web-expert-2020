import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Contact } from '../models/contact.model';
import { ContactService } from '../services/contact.service';


@Component({
    selector: 'app-contact',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.css']
})

export class ContactComponent implements OnInit {
    @Input() contact: Contact;
    @Input() index: number;
    @Output() onUpdate: EventEmitter<any> = new EventEmitter();

    name: string;
    email: string;
    phone: string;
    isFavorite = false;

    ngOnInit() {
        this.name = 'John Doe';
        this.email = 'john.doe@gmail.com';
        this.phone = '011642839';
    }

    constructor(private service: ContactService) { }

    toggleFavorite(index: number) {
        this.service.toggleFavorite(index);
        this.onUpdate.emit();
    }
}
