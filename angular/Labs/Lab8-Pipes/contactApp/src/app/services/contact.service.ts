import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Contact } from '../models/contact.model';

const BASEAPIURL = 'https://webexpert1718.firebaseio.com/contacts.json';
const CONTACTAPIURL = 'https://webexpert1718.firebaseio.com/contacts/';

@Injectable()
export class ContactService {
    contactList: Contact[] = [
        new Contact('jane doe', 'jane.doe@mail.com', '0113448239', true, 'assets/avatar.png'),
        new Contact('john doe', 'john.doe@mail.com', '011424839', false, 'assets/avatar.png'),
        new Contact('Dries Swinnen', 'dries.swinnen@pxl.be', '011664839', true, 'assets/avatar.png')
    ];

    constructor(private http: HttpClient) { }

    getContactList(onlyFavorites: boolean): Observable<Contact[]> {
        return this.http.get(BASEAPIURL).pipe(
            map(this.parseContactData),
            map((contacts: Contact[]) => {
                return onlyFavorites ? this.filterContacts(
                    contacts
                ) : contacts;
            })
        );
    }

    filterContacts(contacts: Contact[]): Contact[] {
        return contacts.filter(
            contact => contact.isFavorite
        );
    }

    parseContactData(rawContacts: any[]): Contact[] {
        return Object.keys(rawContacts).map(key => {
            const contact = rawContacts[key];
            return new Contact(
                contact.name,
                contact.email,
                contact.phone,
                contact.isFavorite,
                contact.avatar,
                key
            );
        });
    }

    addContact(contact: Contact): Observable<any> {
        return this.http
            .post(BASEAPIURL, contact);
    }

    updateContact(id: string, data: any): Observable<any> {
        const url = `${CONTACTAPIURL}${id}.json`;
        return this.http.patch(url, data);
    }

    toggleFavorite(index: number) {
        this.contactList[index].isFavorite = !this.contactList[index].isFavorite;
    }
}
