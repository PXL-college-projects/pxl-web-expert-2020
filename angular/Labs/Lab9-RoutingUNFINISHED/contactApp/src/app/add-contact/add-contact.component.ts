import { ContactService } from '../services/contact.service';
import { Contact } from '../models/contact.model';


export class AddContactComponent {
  created = false;

  constructor(private service: ContactService) { }

  createContact(event: Contact) {
    this.service.addContact(event).subscribe(() => {
      this.created = true;
      setTimeout(() => this.created = false, 5000);
    });
  }
}
