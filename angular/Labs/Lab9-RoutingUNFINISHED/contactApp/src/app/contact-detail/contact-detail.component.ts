import { Router, ActivatedRoute } from '@angular/router';
import { Contact } from '../models/contact.model';
import { ContactService } from '../services/contact.service';
import { OnInit } from '@angular/core';


export class ContactDetailComponent implements OnInit {
  id: string;
  contact: Contact;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: ContactService
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.getContact(this.id);
  }

  getContact(id: string) {
    this.service.getContact(id).subscribe(
      data => this.contact = data
    );
  }

  deleteContact(id: string) {
    this.service.deleteContact(id).subscribe(() => {
      this.deleted = true;
      setTimeout(() => this.router.navigateByUrl(''), 3000);
    });
  }
}
