import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Contact } from '../models/contact.model';

@Component({
    selector: 'app-contact',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.css']
})

export class ContactComponent implements OnInit {
    @Input() contact: Contact;
    @Output() onSubmit: EventEmitter<Contact> = new EventEmitter();
    name: string;
    email: string;
    phone: string;
    isFavorite = false;

    ngOnInit() {
        this.name = 'John Doe';
        this.email = 'john.doe@gmail.com';
        this.phone = '011642839';
    }

    // onClick(): void {
    //     console.log(
    //         'Button clicked. Status van favorite is:' +
    //         this.isFavorite
    //     );
    // }

    submit() {
        this.onSubmit.emit(this.contact);
    }
}
