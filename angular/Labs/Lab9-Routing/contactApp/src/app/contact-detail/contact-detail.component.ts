import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

export class ContactFormComponent implements OnInit {
  @Input() contact: Contact;

  ngOnInit() {
    this.form = new FormGroup({
      'name': new FormControl(this.contact ? this.contact.name : null,
        [Validators.required, Validators.minLength(3)]),
      'email': new FormControl(this.contact ? this.contact.email : null,
        [Validators.required, Validators.pattern(/^[a-z0-9_\.]+@[a-z0-9_\.]+/i)]),
      'phone': new FormControl(this.contact ? this.contact.phone : null,
        [Validators.required, Validators.minLength(9)]),
      'isFavorite': new FormControl(this.contact ? this.contact.isFavorite : false),
      'avatar': new FormControl(this.contact ? this.contact.avatar : null)
    });

  }
}