import { Component, OnInit } from '@angular/core';
import { Contact } from './models/contact.model';
import { ContactService } from './services/contact.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  contactList: Contact[] = [];
  onlyFavorites: boolean;

  ngOnInit() {
    this.fetchContactList(this.onlyFavorites);
  }

  fetchContactList(onlyFavorites: boolean) {
    this.service.getContactList(
      onlyFavorites
    ).subscribe(
      data => {
        this.contactList = data;
      }
    );
  }

  handleData(event: Contact) {
    console.log('Recieved data!', event);
  }

  constructor(private service: ContactService) { }

  createContact(event: Contact) {
    this.service.addContact(event).subscribe(
      () => this.fetchContactList(this.onlyFavorites)
    );
  }

  toggleView(onlyFavorites: boolean): void {
    this.onlyFavorites = !onlyFavorites;
    this.fetchContactList(this.onlyFavorites);
  }

  handleUpdate() {
    this.fetchContactList(this.onlyFavorites);
  }
}
