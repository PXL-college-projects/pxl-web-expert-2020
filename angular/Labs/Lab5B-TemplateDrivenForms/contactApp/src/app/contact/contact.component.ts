import { Component, OnInit, Input } from '@angular/core';
import { Contact } from '../models/contact.model';

@Component({
    selector: 'app-contact',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.css']
})

export class ContactComponent implements OnInit {
    @Input() contact: Contact;
    name: string;
    email: string;
    phone: string;
    isFavorite = false;

    ngOnInit() {
        this.name = 'John Doe';
        this.email = 'john.doe@gmail.com';
        this.phone = '011642839';
    }
}
