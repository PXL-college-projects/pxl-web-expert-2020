import { Component, OnInit } from '@angular/core';
import { Contact } from './models/contact.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  myContact: Contact;
  contactList: Contact[] = [];

  ngOnInit() {
    this.myContact = new Contact(
      'John Doe',
      'john.doe@gmail.com',
      '01166424893',
      true,
      'assets/avatar.png'
    );
    this.createContact(this.myContact);
  }

  handleData(event: Contact) {
    console.log('Recieved data!', event);
  }

  createContact(event: Contact) {
    this.contactList.push(event);
  }
}
