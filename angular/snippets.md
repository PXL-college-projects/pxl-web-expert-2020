# Improve ng serve performance
Nodejs just letted my PC swap and I had to hard reboot.
I have 8G RAM and 1TB diskspace and my system was not responsive.

Conclusion: Node sucks, improve its performance with the tweaks bellow:

## Change angular.json config, and turn the optimizations off

```shell
"optimization": false,
"outputHashing": "all",
"sourceMap": false,
"extractCss": false,
"namedChunks": false,
"aot": false,
"extractLicenses": false,
"vendorChunk": false,
"buildOptimizer": false,
```

## Change the max memory in node
Run the following before you run `ng serve`. Set the number to how much RAM
you want to use, I have 8G and use 6G. I don't want to give node all my RAM.

Quick maths:
`1024(=1G) * 6 = 6144(=6G)`

```shell
# Linux & GNU
export NODE_OPTIONS="--max_old_space_size=6144"
# Windows
set NODE_OPTIONS="--max_old_space_size=6144"
```

You can add it to your bashrc or zshenv on Linux or GNU,
so that you don't have to retype it every time.

```shell
# Bash
echo '# Node stuff' >> ~/.bashrc
echo 'export NODE_OPTIONS="--max_old_space_size=6144"' >> ~/.bashrc
# Zsh
echo '# Node stuff' >> ~/.zshenv
echo 'export NODE_OPTIONS="--max_old_space_size=6144"' >> ~/.zshenv
```

## Vscode extensions which I use
Warning: extensions cause code to crash, I highly recommend that you don't
install any extra extensions, unless you want Vi or Emacs bindings.

This is why code sucks.

```shell
code --install-extension christian-kohler.path-intellisense;
code --install-extension esbenp.prettier-vscode;
code --install-extension johnpapa.Angular2;
code --install-extension ktriek.ng-bootstrap-snippets;
code --install-extension ms-vscode.vscode-typescript-tslint-plugin;
code --install-extension natewallace.angular2-inline;
code --install-extension pflannery.vscode-versionlens;
code --install-extension shd101wyy.markdown-preview-enhanced;
code --install-extension steoates.autoimport;
```

# Snippets
```shell
# Create angular project
ng n <project-name>

# Create component
ng g c components/<component-name>

# Create Service
ng g service services/<service-name>

# Create Pipe
ng g pipe pipes/<pipe-name>

# Create model
ng g class models/<model-name> --type=model

# Create enum
ng g enum enums/<enum-name>


# Forms import
import { FormsModule } from '@angular/forms';
# Add it to imports as well.

# Forms import
import { FormsModule } from '@angular/forms';
# Add it to imports as well.

# HTTP client imports
import { HttpClient, HttpClientModule, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';
# Add those to imports as well.

# Example HTTP client usage
@Injectable()
class myService{
  constructior(private http: HttpClient) {}
}
```