import { element } from 'protractor';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'h45-oefening5';
  private huisdieren: string[] = [];
  private ingegevenHuisdier = '';

  constructor() {
    this.fillHuisdierenArray(['Kat', 'Hond', 'Hamster']);
  }

  private addHuisdier(huisdier: string) {
    this.huisdieren.push(huisdier);
  }

  private removeHuisdierByIndex(index: number) {
    this.huisdieren.splice(index, 1);
  }

  private fillHuisdierenArray(elements: string[]) {
    elements.forEach(
      element => this.huisdieren.push(element)
    );
  }
}
