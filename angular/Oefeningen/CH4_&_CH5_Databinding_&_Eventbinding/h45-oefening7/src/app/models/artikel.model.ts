export class Artikel {
    nummer: number;
    naam: string;
    aankoopprijs: number;
    verkoopprijs: number;
    omschrijving: string;
    afbeeldingURL: string;

    constructor(
        nummer: number,
        naam: string,
        aankoopprijs: number,
        verkoopprijs: number,
        omschrijving: string,
        afbeeldingURL: string
    ) {
        this.nummer = nummer;
        this.naam = naam;
        this.aankoopprijs = aankoopprijs;
        this.verkoopprijs = verkoopprijs;
        this.omschrijving = omschrijving;
        this.afbeeldingURL = afbeeldingURL;
    }
}
