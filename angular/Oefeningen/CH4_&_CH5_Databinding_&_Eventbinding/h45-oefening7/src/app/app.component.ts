import { Component, OnInit } from '@angular/core';
import { Artikel } from './models/artikel.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'h45-oefening7';
  private artikelen = [];

  ngOnInit() {
    this.artikelen = [
      new Artikel(
        1,
        'Tandenborstel',
        1.5,
        3.24,
        'Borstel om je tanden mee proper te maken.',
        'assets/toothbrush.jpg'
      ),
      new Artikel(
        2,
        'Handdoek',
        2.5,
        4.75,
        'Doek om rond je hand te doen.',
        'assets/towel.jpg'
      ),
      new Artikel(
        3,
        'Washandje',
        0.4,
        1.20,
        'Materiaal om je handje mee te wassen.',
        'assets/washcloth.jpg'
      ),
      new Artikel(
        4,
        'Keukenrol',
        1.75,
        0.65,
        'Rol om in de keuken te plaatsen als decoratie.',
        'assets/kitchen_paper.png'
      )
    ];
  }

  private showArticleDetails(index: number) {
    const article = this.artikelen[index];

    alert(
      article.omschrijving + ' - ' +
      article.verkoopprijs + ' - ' +
      article.aankoopprijs
    );
  }
}
