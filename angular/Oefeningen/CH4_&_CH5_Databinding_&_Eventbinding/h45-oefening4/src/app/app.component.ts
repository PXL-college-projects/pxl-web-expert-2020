import { PokemonType } from './enums/pokemon-type.enum';
import { Component } from '@angular/core';
import { Pokemon } from './models/pokemon.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'h45-oefening4';
  private selectedPokemonId;
  private pokemons = [
    new Pokemon(1, 'Bulbasaur', PokemonType.GRASS, 'assets/bulbasaur.png'),
    new Pokemon(2, 'Ivysaur', PokemonType.GRASS, 'assets/ivysaur.png'),
    new Pokemon(3, 'Venusaur', PokemonType.GRASS, 'assets/venusaur.png'),
    new Pokemon(4, 'Charmander', PokemonType.FIRE, 'assets/charmander.png'),
    new Pokemon(5, 'Charmeleon', PokemonType.FIRE, 'assets/charmeleon.png'),
    new Pokemon(6, 'Charizard', PokemonType.FIRE, 'assets/charizard.png'),
    new Pokemon(7, 'Squirtle', PokemonType.WATER, 'assets/squirtle.png'),
    new Pokemon(8, 'Wartortle', PokemonType.WATER, 'assets/wartortle.png'),
    new Pokemon(9, 'Blastoise', PokemonType.WATER, 'assets/blastoise.png')
  ];

  private getPokemonBorderStyle(pokemonId: number) {
    if (pokemonId === this.selectedPokemonId) {
      return '3px solid yellow';
    } else {
      return '1px solid #ccc';
    }
  }

  private getBackgroundColor(pokemonType: PokemonType) {
    switch (pokemonType) {
      case PokemonType.GRASS: return 'green';
      case PokemonType.WATER: return 'blue';
      case PokemonType.FIRE: return 'red';
      default: return 'white';
    }
  }

  private getPokemonById(pokemonId: number) {
    return this.pokemons.find(
      pokemon => pokemon.id === pokemonId
    );
  }

  private handlePokemonOnClick(id: number) {
    this.selectedPokemonId = id;
  }
}
