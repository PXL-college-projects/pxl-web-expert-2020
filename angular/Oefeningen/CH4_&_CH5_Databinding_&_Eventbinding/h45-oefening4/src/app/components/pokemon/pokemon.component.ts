import { Pokemon } from './../../models/pokemon.model';
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['./pokemon.component.css']
})
export class PokemonComponent {
  @Input() private pokemon: Pokemon = null;
  @Output() private pokemonClickedEmitter = new EventEmitter();

  private pokemonClicked() {
    this.pokemonClickedEmitter.emit(this.pokemon.id);
  }
}
