import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  private showDetails = false;
  private log: string[];

  constructor() {
    this.log = [];
  }

  title = 'h45-oefening2';
  onClick() {
    this.showDetails = !this.showDetails;
    this.log.push(new Date().getTime().toString());
  }
}
