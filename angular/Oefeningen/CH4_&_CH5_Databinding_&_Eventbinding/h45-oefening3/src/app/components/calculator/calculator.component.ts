import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent implements OnInit {
  private number1: number;
  private number2: number;
  private result: number;

  constructor() { }

  ngOnInit() {
  }

  add() {
    this.result = this.number1 + this.number2;
  }

}
