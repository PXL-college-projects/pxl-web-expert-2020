import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  private bericht: string;

  constructor() { }

  ngOnInit() {
  }

  verstuur() {
    alert('bericht ontvangen:' + this.bericht);
  }

}
