import { Injectable } from '@angular/core';
import { Customer } from '../models/customer.model';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  private customers = [
    new Customer(
      654981,
      'Jos',
      'De Zotte',
      'zot@ditIsMijnEmail.com',
      'De straat van Jos',
      8,
      5461,
      'Gekke Gemeente'
    ),
    new Customer(
      6581,
      'Jan',
      'De Toffe',
      'Jan@jan.com',
      'De straat van Jan',
      5,
      6541,
      'Jan\'s Gemeente'
    )
  ];

  getCustomers() {
    return this.customers;
  }

  removeCustomerById(id: number) {
    const indexOfCustomerToRemove = this.customers.indexOf(
      this.getCustomerById(id)
    );

    if (indexOfCustomerToRemove !== -1) {
      this.customers.splice(indexOfCustomerToRemove, 1);
    }
  }

  addCustomer(customer: Customer) {
    this.customers.push(customer);
  }

  private getCustomerById(id: number) {
    return this.customers.find(
      customer => customer.id === id
    );
  }
}
