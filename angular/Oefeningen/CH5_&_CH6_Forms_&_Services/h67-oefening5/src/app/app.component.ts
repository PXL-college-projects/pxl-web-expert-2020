import { Component } from '@angular/core';
import { CustomerService } from './services/customer.service';
import { Customer } from './models/customer.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'h67-oefening5';
  constructor(private customerService: CustomerService) { }

  onCustomerFormSubmit(customer: Customer) {
    this.customerService.addCustomer(
      customer
    );
  }
}
