import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Customer } from 'src/app/models/customer.model';
import { validateCountryCode } from 'src/app/validators/countrycode.validator';

@Component({
  selector: 'app-customer-form',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})
export class CustomerFormComponent implements OnInit {
  private newCustomerForm: FormGroup;
  @Output() formSubmitted = new EventEmitter();

  ngOnInit() {
    this.newCustomerForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      lastname: new FormControl('', [Validators.required]),
      email: new FormControl('@email.com', [Validators.required, Validators.email]),
      street: new FormControl('', [Validators.required]),
      streetnumber: new FormControl('', [Validators.required]),
      countrycode: new FormControl('', [Validators.required, validateCountryCode]),
      country: new FormControl('', [Validators.required]),
    });
  }

  onSubmit() {
    this.formSubmitted.emit(
      new Customer(
        Math.random(),
        this.newCustomerForm.value.name,
        this.newCustomerForm.value.lastname,
        this.newCustomerForm.value.email,
        this.newCustomerForm.value.street,
        this.newCustomerForm.value.streetnumber,
        this.newCustomerForm.value.countrycode,
        this.newCustomerForm.value.country
      )
    );
  }
}
