import { Customer } from './../../models/customer.model';
import { Component, Input } from '@angular/core';
import { CustomerService } from 'src/app/services/customer.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent {
  @Input() private customer: Customer;
  constructor(private customerService: CustomerService) { }

  remove() {
    this.customerService.removeCustomerById(
      this.customer.id
    );
  }

}
