export class Customer {
    id: number;
    voornaam: string;
    achternaam: string;
    email: string;
    straatnaam: string;
    huisnummer: number;
    postcode: number;
    gemeente: string;

    constructor(
        id: number,
        voornaam: string,
        achternaam: string,
        email: string,
        straatnaam: string,
        huisnummer: number,
        postcode: number,
        gemeente: string
    ) {
        this.id = id;
        this.voornaam = voornaam;
        this.achternaam = achternaam;
        this.email = email;
        this.straatnaam = straatnaam;
        this.huisnummer = huisnummer;
        this.postcode = postcode;
        this.gemeente = gemeente;
    }
}
