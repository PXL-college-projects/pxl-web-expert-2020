import { FormControl } from '@angular/forms';

export function validateCountryCode(control: FormControl) {
    if (control.value > 1000 && control.value < 8000) {
        return null;
    } else {
        return { validateCountryCode: { valid: false } };
    }
}
