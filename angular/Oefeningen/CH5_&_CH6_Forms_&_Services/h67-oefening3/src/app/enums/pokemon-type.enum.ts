export enum PokemonType {
    GRASS = 'Grass',
    FIRE = 'Fire',
    WATER = 'Water'
}
