import { Injectable } from '@angular/core';
import { Pokemon } from '../models/pokemon.model';
import { PokemonType } from '../enums/pokemon-type.enum';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  private pokemons = [
    new Pokemon(1, 'Bulbasaur', PokemonType.GRASS, 'assets/bulbasaur.png'),
    new Pokemon(2, 'Ivysaur', PokemonType.GRASS, 'assets/ivysaur.png'),
    new Pokemon(3, 'Venusaur', PokemonType.GRASS, 'assets/venusaur.png'),
    new Pokemon(4, 'Charmander', PokemonType.FIRE, 'assets/charmander.png'),
    new Pokemon(5, 'Charmeleon', PokemonType.FIRE, 'assets/charmeleon.png'),
    new Pokemon(6, 'Charizard', PokemonType.FIRE, 'assets/charizard.png'),
    new Pokemon(7, 'Squirtle', PokemonType.WATER, 'assets/squirtle.png'),
    new Pokemon(8, 'Wartortle', PokemonType.WATER, 'assets/wartortle.png'),
    new Pokemon(9, 'Blastoise', PokemonType.WATER, 'assets/blastoise.png')
  ];

  private getPokemons() {
    return this.pokemons;
  }

  private getPokemonNameById(id: number) {
    return this.getPokemonById(
      id
    ).name;
  }

  private getBackgroundColor(pokemonType: PokemonType) {
    switch (pokemonType) {
      case PokemonType.GRASS: return 'green';
      case PokemonType.WATER: return 'blue';
      case PokemonType.FIRE: return 'red';
      default: return 'white';
    }
  }

  getPokemonById(pokemonId: number) {
    return this.pokemons.find(
      pokemon => pokemon.id === pokemonId
    );
  }

  addPokemon(pokemon: Pokemon) {
    this.pokemons.push(pokemon);
    this.pokemons.sort();
  }
}
