import { PokemonService } from './services/pokemon.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'h45-oefening4';
  private selectedPokemonId: number;
  constructor(private pokemonService: PokemonService) {}

  private handlePokemonOnClick(id: number) {
    this.selectedPokemonId = id;
  }

  private getSelectedPokemonName() {
    return this.pokemonService.getPokemonById(
      this.selectedPokemonId
    ).name;
  }

  private getPokemonBorderStyle(pokemonId: number) {
    if (pokemonId === this.selectedPokemonId) {
      return '3px solid yellow';
    } else {
      return '1px solid #ccc';
    }
  }
}
