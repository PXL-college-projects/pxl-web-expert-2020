import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPokemonFormComponent } from './new-pokemon-form.component';

describe('NewPokemonFormComponent', () => {
  let component: NewPokemonFormComponent;
  let fixture: ComponentFixture<NewPokemonFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPokemonFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPokemonFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
