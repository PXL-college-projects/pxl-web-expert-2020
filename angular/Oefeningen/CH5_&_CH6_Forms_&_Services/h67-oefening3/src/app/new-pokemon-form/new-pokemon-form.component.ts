import { PokemonType } from './../enums/pokemon-type.enum';
import { Pokemon } from './../models/pokemon.model';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PokemonService } from '../services/pokemon.service';
import { validateUniqueId } from '../validators/unique-id.validator';

@Component({
  selector: 'app-new-pokemon-form',
  templateUrl: './new-pokemon-form.component.html',
  styleUrls: ['./new-pokemon-form.component.css']
})
export class NewPokemonFormComponent implements OnInit {
  private pokemonTypes = Object.values(PokemonType);
  private newPokemonForm: FormGroup;

  constructor(private pokemonService: PokemonService) { }

  ngOnInit() {
    this.newPokemonForm = new FormGroup({
      id: new FormControl('', [Validators.required, Validators.min(0), validateUniqueId]),
      name: new FormControl('', [Validators.required]),
      type: new FormControl(PokemonType.GRASS, [Validators.required]),
      icon: new FormControl('', [Validators.required, Validators.pattern('assets/.+.png')]),
    });
  }

  onSubmit() {
    this.pokemonService.addPokemon(
      new Pokemon(
        this.newPokemonForm.value.id,
        this.newPokemonForm.value.name,
        this.newPokemonForm.value.type,
        this.newPokemonForm.value.icon
      )
    );
  }
}
