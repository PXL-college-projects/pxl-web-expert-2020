import { PokemonType } from '../enums/pokemon-type.enum';

export class Pokemon {
    id: number;
    name: string;
    type: PokemonType;
    icon: string;

    constructor(
        id: number,
        name: string,
        type: PokemonType,
        icon: string
    ) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.icon = icon;
    }
}
