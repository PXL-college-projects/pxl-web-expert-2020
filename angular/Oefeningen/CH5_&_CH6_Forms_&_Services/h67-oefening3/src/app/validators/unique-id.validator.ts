import { PokemonService } from './../services/pokemon.service';
import { FormControl } from '@angular/forms';

export function validateUniqueId(control: FormControl) {
    if (new PokemonService().getPokemonById(control.value) == null) {
        return null;
    } else {
        return { validateUniqueId: { valid: false } };
    }
}
