import { MeetingType } from './../../../models/meeting-type.model';
import { Component } from '@angular/core';
import { Meeting } from 'src/app/models/meeting.model';

@Component({
  selector: 'app-meeting-form',
  templateUrl: './meeting-form.component.html',
  styleUrls: ['./meeting-form.component.css']
})
export class MeetingFormComponent {
  private meetingTypes = Object.values(MeetingType);
  private meeting = new Meeting();

  onSubmit(meetingForm) {
    console.log(this.meeting);
  }
}
