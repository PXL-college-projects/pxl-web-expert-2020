export enum MeetingType {
    ONTSPANNING = 'Ontspanning',
    ZAKELIJK = 'Zakelijk',
    ANDERE = 'Andere'
}
