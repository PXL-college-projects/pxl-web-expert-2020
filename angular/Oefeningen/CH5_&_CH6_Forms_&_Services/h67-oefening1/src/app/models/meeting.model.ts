import { MeetingType } from './meeting-type.model';

export class Meeting {
    beschrijving: string;
    date: Date;
    meetingType: MeetingType;
    reminder: boolean;

    constructor() {
        this.meetingType = MeetingType.ONTSPANNING;
    }
}
