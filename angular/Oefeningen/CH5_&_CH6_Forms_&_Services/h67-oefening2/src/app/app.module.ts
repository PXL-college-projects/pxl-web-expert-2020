import { GenerateNumberWithProviderComponent } from './generate-number-with-provider/generate-number-with-provider.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { GenerateNumberComponent } from './generate-number/generate-number.component';

@NgModule({
  declarations: [
    AppComponent,
    GenerateNumberComponent,
    GenerateNumberWithProviderComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
