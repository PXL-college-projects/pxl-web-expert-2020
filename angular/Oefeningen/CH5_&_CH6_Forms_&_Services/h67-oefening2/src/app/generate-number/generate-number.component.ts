import { NumberService } from '../number.service';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-generate-number',
  templateUrl: './generate-number.component.html',
  styleUrls: ['./generate-number.component.css']
})
export class GenerateNumberComponent {
  private id = 0;
  @Input() private name = '';

  constructor(private numberService: NumberService) { }

  onClick() {
    this.id = this.numberService.next();
  }
}
