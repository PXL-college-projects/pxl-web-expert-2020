import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NumberService {
  private id: number = 0;
  constructor() { }

  next() : number {
    this.id++;
    return this.id;
  }
}
