import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateNumberWithProviderComponent } from './generate-number-with-provider.component';

describe('GenerateNumberWithProviderComponent', () => {
  let component: GenerateNumberWithProviderComponent;
  let fixture: ComponentFixture<GenerateNumberWithProviderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateNumberWithProviderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateNumberWithProviderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
