import { NumberService } from '../number.service';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-generate-number-with-provider',
  templateUrl: './generate-number-with-provider.component.html',
  styleUrls: ['./generate-number-with-provider.component.css'],
  providers: [NumberService]
})
export class GenerateNumberWithProviderComponent {
  private id = 0;
  @Input() private name = '';

  constructor(private numberService: NumberService) { }

  onClick() {
    this.id = this.numberService.next();
  }
}
