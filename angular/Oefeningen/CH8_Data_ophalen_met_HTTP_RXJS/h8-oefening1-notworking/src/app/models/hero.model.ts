export class Hero {
    abilities: string[];
    avatarImageURL: string;
    heroClass: string;
    rating: number;
    name: string;

    constructor(
        abilities: string[],
        avatarImageURL: string,
        heroClass: string,
        rating: number,
        name: string
    ) {
        this.abilities = abilities;
        this.avatarImageURL = avatarImageURL;
        this.heroClass = heroClass;
        this.rating = rating;
        this.name = name;
    }
}
