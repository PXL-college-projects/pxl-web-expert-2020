import { Component, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  @Output() filterEmitter = new EventEmitter<object>();

  nameFilter: string;
  minRating: number;
  constructor() { }

  ngOnInit() {
    this.nameFilter = '';
    this.minRating = 0;
  }

  onChangeFilter(event) {
    console.log(event, this.nameFilter, this.minRating);
    this.filterEmitter.emit({ name: this.nameFilter, rating: this.minRating });
  }

}
