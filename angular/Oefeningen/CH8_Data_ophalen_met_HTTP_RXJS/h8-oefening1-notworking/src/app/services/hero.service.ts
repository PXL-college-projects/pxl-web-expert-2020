import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Hero } from '../models/hero.model';

const BASEAPIURL = 'assets/heroes.json';

@Injectable({
  providedIn: 'root'
})
export class HeroService {


constructor(private http: HttpClient) { }

getHeroes(): Observable<Hero[]> {
    return this.http.get(BASEAPIURL).pipe(
        map(this.parseHeroData)
    );
}

parseHeroData(rawHeroes: any[]): Hero[] {
    return Object.keys(rawHeroes).map(key => {
        const hero = rawHeroes[key];
        return new Hero(
            hero.abilities,
            hero.avatar,
            hero.class,
            hero.difficulty,
            hero.name
        );
    });
}
}
