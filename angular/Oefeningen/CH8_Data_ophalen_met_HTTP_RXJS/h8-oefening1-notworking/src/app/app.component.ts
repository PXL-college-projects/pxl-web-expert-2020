import { HeroService } from './services/hero.service';
import { Component, OnInit } from '@angular/core';
import { Hero } from './models/hero.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'h8-oefening1';
  constructor(private heroService: HeroService) {
    this.heroService = heroService;
   }
  nameFilter: string;
  minRating: number;
  heroes: Hero[];
  shownHeroes: Hero[];

  ngOnInit() {
    this.heroService.getHeroes().subscribe(data => {
      this.heroes = data;
      this.runFilter();
    });

    this.nameFilter = '';
    this.minRating = 0;
    this.heroes = [];
    this.runFilter();
  }

  runFilter() {
    this.shownHeroes = this.heroes
      .filter(h => h.name.includes(this.nameFilter))
      .filter(h => h.rating > this.minRating);
  }

  setFilter(o: object) {
    this.nameFilter = o['name'];
    this.minRating = o['rating'];
    this.runFilter();
  }

}
